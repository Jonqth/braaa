//
//  main.m
//  Braaa
//
//  Created by Jonathan Araujo-Levy on 30/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BRAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BRAppDelegate class]));
    }
}
