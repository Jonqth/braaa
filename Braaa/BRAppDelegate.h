//
//  BRAppDelegate.h
//  Braaa
//
//  Created by Jonathan Araujo-Levy on 30/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BRMainViewController;

@interface BRAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) BRMainViewController *mainViewController;

@end
