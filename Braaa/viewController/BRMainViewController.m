//
//  BRMainViewController.m
//  Braaa
//
//  Created by Jonathan Araujo-Levy on 30/12/12.
//  Copyright (c) 2012 coesense. All rights reserved.
//

#import "BRMainViewController.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "BRTorch.h"

@interface BRMainViewController () {
    SystemSoundID audioEffect;
    Torch *torch;
    UIButton *BRButton;
}

@end

@implementation BRMainViewController

- (id)init {
    
    // Audio Settings
    AudioServicesDisposeSystemSoundID(audioEffect);
    
    // Torch
    torch = [Torch sharedInstance];
    
    self = [super init];
    if (self) { }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self BRUILoading];
}

- (void) BRUILoading {
    
    // BgView
    UIImage *BRBackButtonImage = [UIImage imageNamed:@"BR_bgButton.png"];
    UIImageView *BRBackButtonView = [[UIImageView alloc] init];
    
    [BRBackButtonView setImage:BRBackButtonImage];
    [BRBackButtonView setFrame:CGRectMake((self.view.frame.size.width/2)-(BRBackButtonImage.size.width/2),
                                          (self.view.frame.size.height/2)-(BRBackButtonImage.size.height/2),
                                          BRBackButtonImage.size.width, BRBackButtonImage.size.height)];
    
    // BraaaButton
    BRButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *BRButtonImageNormal = [UIImage imageNamed:@"BR_buttonNormal.png"];
    UIImage *BRButtonImageHighlighted = [UIImage imageNamed:@"BR_buttonHighlight.png"];
    [BRButton setFrame:CGRectMake((self.view.frame.size.width/2)-(BRButtonImageNormal.size.width/2),
                                  (self.view.frame.size.height/2)-(BRButtonImageNormal.size.height/2),
                                  BRButtonImageNormal.size.width, BRButtonImageNormal.size.height)];
    
    
    [BRButton setBackgroundImage:BRButtonImageNormal forState:UIControlStateNormal];
    [BRButton setBackgroundImage:BRButtonImageHighlighted forState:UIControlStateHighlighted];
    [BRButton addTarget:self action:@selector(BRSoundPlay) forControlEvents:UIControlEventTouchUpInside];
    
    
    // TitleView
    UIImage *BRTitleImage = [UIImage imageNamed:@"BR_downTitle.png"];
    UIImageView *BRTitleView = [[UIImageView alloc] init];
    
    [BRTitleView setImage:BRTitleImage];
    [BRTitleView setFrame:CGRectMake((self.view.frame.size.width/2)-(BRTitleImage.size.width/2),
                                          (BRBackButtonView.frame.origin.y+BRBackButtonImage.size.height)+(20),
                                          BRTitleImage.size.width, BRTitleImage.size.height)];
    
    [self.view addSubview:BRTitleView];
    [self.view addSubview:BRBackButtonView];
    [self.view addSubview:BRButton];
}

- (void) BRSoundPlay {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"braaa" ofType:@"mp3"];
    if ([[NSFileManager defaultManager] fileExistsAtPath : path]) {
        NSURL *pathURL = [NSURL fileURLWithPath : path];
        AudioServicesCreateSystemSoundID((CFURLRef) CFBridgingRetain(pathURL), &audioEffect);
        AudioServicesPlaySystemSound(audioEffect);
    }
    else {
        NSLog(@"error, file not found: %@", path);
    }
    
    [self BRTorchOn];
    [self BRAnimate];
}

- (void) BRAnimate {
    
    UIImage *first = [UIImage imageNamed:@"BR_anim1.png"];
    UIImage *second = [UIImage imageNamed:@"BR_anim2.png"];
    UIImage *third = [UIImage imageNamed:@"BR_anim3.png"];
    
    UIImageView *containImage = [[UIImageView alloc] init];
    [self.view addSubview:containImage];
    
    UIImageView *containImage_ = [[UIImageView alloc] init];
    [self.view addSubview:containImage_];
    
    UIImageView *containImage__ = [[UIImageView alloc] init];
    [self.view addSubview:containImage__];
    
    [UIView animateWithDuration:0.3 animations:^{
        [containImage setFrame:CGRectMake(self.view.frame.size.width/2-(first.size.width/2), (BRButton.frame.origin.y-(first.size.height+15)),
                                          first.size.width, first.size.height)];
        [containImage setImage:first];
        
    } completion:^(BOOL finished) {
        [containImage setImage:nil];
        
            [UIView animateWithDuration:0.3 animations:^{
                [containImage_ setFrame:CGRectMake(self.view.frame.size.width/2-(second.size.width/2), (containImage.frame.origin.y-(second.size.height+15)),
                                                   second.size.width, second.size.height)];
                [containImage_ setImage:second];
            
            } completion:^(BOOL finished) {
                [containImage_ setImage:nil];
                
                    [UIView animateWithDuration:0.3 animations:^{
                        [containImage__ setFrame:CGRectMake(self.view.frame.size.width/2-(third.size.width/2), (containImage_.frame.origin.y-(third.size.height+15)),
                                                            third.size.width, third.size.height)];
                        [containImage__ setImage:third];
                    
                    } completion:^(BOOL finished) {
                        [containImage__ setImage:nil];
                    }];
            }];
    }];
    
}

- (void) BRTorchOn {
    if([torch isTorchOn]) { [torch stopStrobe]; }
	else { [torch startStrobesPerSecond:10 forNumberOfSeconds:1]; }
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
